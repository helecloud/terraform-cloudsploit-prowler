# tf-stack

Example to show Terraform Enterprise/Cloud in use.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ami | AMI of image | string | `"ami-0c3f128b7298d29b9"` | no |
| project\_tags | Project tags to be used to track costs. | map | `<map>` | no |
| region |  | string | `"eu-west-2"` | no |
